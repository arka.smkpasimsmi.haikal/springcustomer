package com.haikal.customerLagi.dao;

import com.haikal.customerLagi.model.Customer;
import com.haikal.customerLagi.repository.CustomerRepository;
import com.haikal.customerLagi.service.CustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CustomerDao implements CustomerService {

    @Autowired
    CustomerRepository customerRepository;

    @Override
    public void saveCust(Customer customer) {
        this.customerRepository.save(customer);
    }

    @Override
    public List<Customer> getAllCust() {
        return customerRepository.findAll();
    }

    @Override
    public void delete(String id) {
        this.customerRepository.deleteById(id);
    }
}
