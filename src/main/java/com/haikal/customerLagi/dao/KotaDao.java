package com.haikal.customerLagi.dao;

import com.haikal.customerLagi.model.Kota;
import com.haikal.customerLagi.repository.KotaRepository;
import com.haikal.customerLagi.service.KotaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class KotaDao implements KotaService {

    @Autowired
    KotaRepository kotaRepository;

    @Override
    public void saveKota(Kota kota) {
        this.kotaRepository.save(kota);
    }

    @Override
    public List<Kota> getAllKota() {
        return kotaRepository.findAll();
    }

    @Override
    public void delete(String id) {
        this.kotaRepository.deleteById(id);
    }
}
