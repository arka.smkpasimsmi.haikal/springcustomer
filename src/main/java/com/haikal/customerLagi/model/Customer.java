package com.haikal.customerLagi.model;

import com.haikal.customerLagi.generator.StringPrefixedSequenceIdGenerator;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;

@Entity
@Table(name = "customer")
public class Customer {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "customer_seq")
    @GenericGenerator(
            name = "customer_seq",
            strategy = "com.haikal.customerLagi.generator.StringPrefixedSequenceIdGenerator",
            parameters = {
                    @org.hibernate.annotations.Parameter(name = StringPrefixedSequenceIdGenerator.INCREMENT_PARAM, value = "6"),
                    @org.hibernate.annotations.Parameter(name = StringPrefixedSequenceIdGenerator.VALUE_PREFIX_PARAMETER, value = ""),
                    @org.hibernate.annotations.Parameter(name = StringPrefixedSequenceIdGenerator.NUMBER_FORMAT_PARAMETER, value = "%06d")
            })
    @Column(columnDefinition = "char(6)")
    private String id;

    @Column(length = 50)
    private String nama;
    private String Alamat;

    @ManyToOne
    @JoinColumn(name = "kotaid", nullable = false)
    private Kota kota;
    private Double pendapatan;

    public Customer(String nama, String alamat, Kota kota, Double pendapatan) {
        this.nama = nama;
        Alamat = alamat;
        this.kota = kota;
        this.pendapatan = pendapatan;
    }

    public Customer(){

    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getAlamat() {
        return Alamat;
    }

    public void setAlamat(String alamat) {
        Alamat = alamat;
    }

    public Kota getKota() {
        return kota;
    }

    public void setKota(Kota kota) {
        this.kota = kota;
    }

    public Double getPendapatan() {
        return pendapatan;
    }

    public void setPendapatan(Double pendapatan) {
        this.pendapatan = pendapatan;
    }

    @Override
    public String toString() {
        return "Customer{" +
                "id='" + id + '\'' +
                ", nama='" + nama + '\'' +
                ", Alamat='" + Alamat + '\'' +
                ", kota=" + kota +
                ", pendapatan=" + pendapatan +
                '}';
    }
}
