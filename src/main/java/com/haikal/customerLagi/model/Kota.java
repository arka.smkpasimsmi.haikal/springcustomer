package com.haikal.customerLagi.model;

import com.haikal.customerLagi.generator.StringPrefixedSequenceIdGenerator;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Entity
@Table(name = "kota")
public class Kota {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "kota_seq")
    @GenericGenerator(
            name = "kota_seq",
            strategy = "com.haikal.customerLagi.generator.StringPrefixedSequenceIdGenerator",
            parameters = {
                    @Parameter(name = StringPrefixedSequenceIdGenerator.INCREMENT_PARAM, value = "3"),
                    @Parameter(name = StringPrefixedSequenceIdGenerator.VALUE_PREFIX_PARAMETER, value = ""),
                    @Parameter(name = StringPrefixedSequenceIdGenerator.NUMBER_FORMAT_PARAMETER, value = "%03d")
            })
    @Column(columnDefinition = "char(3)")
    private String id;

    @Column(name = "nama", length = 50)
    @NotNull
    @Size(min=3, max=32)
    private String nama;


    public Kota(String nama) {
        this.nama = nama;
    }

    public Kota(){

    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    @Override
    public String toString() {
        return "Siswa{"+
                "id=" + id +
                ", nama='" + nama + '\'' +
                '}';
    }
}
