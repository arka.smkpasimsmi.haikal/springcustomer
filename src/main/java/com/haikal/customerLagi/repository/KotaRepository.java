package com.haikal.customerLagi.repository;

import com.haikal.customerLagi.model.Kota;
import org.springframework.data.jpa.repository.JpaRepository;

public interface KotaRepository extends JpaRepository<Kota,String> {
}
