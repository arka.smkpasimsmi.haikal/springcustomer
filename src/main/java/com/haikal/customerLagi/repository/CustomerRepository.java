package com.haikal.customerLagi.repository;

import com.haikal.customerLagi.model.Customer;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CustomerRepository extends JpaRepository<Customer,String> {
}
