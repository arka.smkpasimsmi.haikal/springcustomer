package com.haikal.customerLagi.controller;

import com.haikal.customerLagi.model.Kota;
import com.haikal.customerLagi.service.KotaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

@Controller
public class KotaController {

    @Autowired
    private KotaService kotaService;

    @GetMapping("/kota")
    public String KotaList(Model model){
        model.addAttribute("addKota",new Kota());
        model.addAttribute("listKota",kotaService.getAllKota());
        return "kota";
    }

    @PostMapping("/kota/create")
    public String buatKota(@ModelAttribute("addKota") Kota kota){
        kotaService.saveKota(kota);
        return "redirect:/kota";
    }

    @GetMapping("/kota/hapus/{id}")
    public String hapusKota(@PathVariable String id){
        kotaService.delete(id);
        return "redirect:/kota";
    }
}
