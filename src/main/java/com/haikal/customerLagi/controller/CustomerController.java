package com.haikal.customerLagi.controller;

import com.haikal.customerLagi.model.Customer;
import com.haikal.customerLagi.service.CustReportService;
import com.haikal.customerLagi.service.CustomerService;
import com.haikal.customerLagi.service.KotaService;
import net.sf.jasperreports.engine.JRException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.io.FileNotFoundException;

@Controller
public class CustomerController {

    @Autowired
    CustomerService customerService;

    @Autowired
    KotaService kotaService;

    @Autowired
    CustReportService custReportService;

    @GetMapping(value = {"/","/customer"})
    public String CustList(Model model){
        model.addAttribute("addCust",new Customer());
        model.addAttribute("listCust",customerService.getAllCust());
        model.addAttribute("listKota",kotaService.getAllKota());
        return "customer";
    }

    @PostMapping("/customer/create")
    public String buatCust(@ModelAttribute("addCust") Customer customer){
        customerService.saveCust(customer);
        return "redirect:/customer";
    }

    @GetMapping("/customer/report/{format}")
    public String buatCustLaporan(@PathVariable String format) throws FileNotFoundException, JRException {
        return custReportService.exportReport(format);
    }

    @GetMapping(value = "/customer/hapus/{id}")
    public String hapusCust(@PathVariable String id){
        customerService.delete(id);
        return"redirect:/customer";
    }
}
