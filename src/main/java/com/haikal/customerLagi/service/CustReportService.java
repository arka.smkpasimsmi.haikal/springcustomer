package com.haikal.customerLagi.service;

import com.haikal.customerLagi.model.Customer;
import net.sf.jasperreports.engine.*;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import net.sf.jasperreports.engine.design.JasperDesign;
import org.springframework.stereotype.Service;
import org.springframework.util.ResourceUtils;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class CustReportService {
    private CustomerService customerService;

    public CustReportService(CustomerService customerService) {
        this.customerService = customerService;
    }

    public String exportReport(String reportFormat) throws FileNotFoundException, JRException {
        String path="D:\\";
        List<Customer> customers = customerService.getAllCust();
        File file = ResourceUtils.getFile("classpath:report.jrxml");
//        JasperDesign jasperDesign;
        JasperReport jasperReport= JasperCompileManager.compileReport(file.getAbsolutePath());
        JRBeanCollectionDataSource dataSource = new JRBeanCollectionDataSource(customers);
        Map<String,Object> parameter=new HashMap<>();
        parameter.put("createdBy","PT Collega");
        JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport,parameter,dataSource);
        if (reportFormat.equalsIgnoreCase("html")){
            JasperExportManager.exportReportToHtmlFile(jasperPrint,path+"laporan_customer.html");
        }
        if (reportFormat.equalsIgnoreCase("pdf")){
            JasperExportManager.exportReportToPdfFile(jasperPrint,path+"laporan_customer.pdf");
        }
        return "redirect:/customer";
    }
}
