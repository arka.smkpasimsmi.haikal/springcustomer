package com.haikal.customerLagi.service;

import com.haikal.customerLagi.model.Customer;

import java.util.List;

public interface CustomerService {
    void saveCust(Customer customer);
    List<Customer> getAllCust();
    void delete(String id);
}
