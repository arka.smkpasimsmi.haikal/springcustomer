package com.haikal.customerLagi.service;

import com.haikal.customerLagi.model.Kota;

import java.util.List;

public interface KotaService {
    void saveKota(Kota kota);
    List<Kota> getAllKota();
    void delete(String id);
}
