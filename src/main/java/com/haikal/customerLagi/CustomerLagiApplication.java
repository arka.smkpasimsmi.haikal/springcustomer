package com.haikal.customerLagi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CustomerLagiApplication {

	public static void main(String[] args) {
		SpringApplication.run(CustomerLagiApplication.class, args);
	}

}
